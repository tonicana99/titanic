import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    df['Title'] = df['Name'].str.extract(r',\s(.*?\.)\s')

    prefixes = ['Mr.', 'Mrs.', 'Miss.']
    dfs_by_prefix = {}

    for prefix in prefixes:
        filtered_df = df[df['Title'].str.startswith(prefix)]
        dfs_by_prefix[prefix] = filtered_df

    lista = []

    for pref in prefixes:
        toupletmp = (pref, dfs_by_prefix[pref]["Age"].isna().sum(), int(dfs_by_prefix[pref]["Age"].median()))
        lista.append(toupletmp)
    return lista
